# VESPER QGIS plugin

This plugin provides a powerful graphical user interface and allows the integration of [VESPER](https://gitlab.com/spadarian/vesper) into more complex GIS workflows within QGIS.

### What is VESPER?

VESPER (Variogram Estimation and Spatial Prediction with ERror) is a spatial interpolation package that implements Kriging with local variograms.

The original software was developed by the Australian Centre for Precision Agriculture of The University of Sydney and it has been re-implemented in Python as an open-source project available at: https://gitlab.com/spadarian/vesper

For more information, refer to the [original user manual ![pdf_icon](https://upload.wikimedia.org/wikipedia/commons/2/23/Icons-mini-file_acrobat.gif)](https://precision-agriculture.sydney.edu.au/wp-content/uploads/2019/08/Vesper_1.6_User_Manual.pdf) and [VESPER code repository](https://gitlab.com/spadarian/vesper).


