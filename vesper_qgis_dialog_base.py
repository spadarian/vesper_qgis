# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'vesper_qgis_dialog_base.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_VESPERDialogBase(object):
    def setupUi(self, VESPERDialogBase):
        VESPERDialogBase.setObjectName("VESPERDialogBase")
        VESPERDialogBase.resize(400, 413)
        self.button_box = QtWidgets.QDialogButtonBox(VESPERDialogBase)
        self.button_box.setGeometry(QtCore.QRect(30, 360, 341, 32))
        self.button_box.setOrientation(QtCore.Qt.Horizontal)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.formLayoutWidget = QtWidgets.QWidget(VESPERDialogBase)
        self.formLayoutWidget.setGeometry(QtCore.QRect(20, 150, 351, 161))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.n_points = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.n_points.setToolTip("")
        self.n_points.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.n_points.setInputMask("")
        self.n_points.setText("20")
        self.n_points.setMaxLength(4)
        self.n_points.setObjectName("n_points")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.n_points)
        self.label_2 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.variogram_model = QtWidgets.QComboBox(self.formLayoutWidget)
        self.variogram_model.setObjectName("variogram_model")
        self.variogram_model.addItem("")
        self.variogram_model.addItem("")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.variogram_model)
        self.label_points_2 = QtWidgets.QLabel(VESPERDialogBase)
        self.label_points_2.setGeometry(QtCore.QRect(20, 120, 172, 30))
        self.label_points_2.setObjectName("label_points_2")
        self.layoutWidget_2 = QtWidgets.QWidget(VESPERDialogBase)
        self.layoutWidget_2.setGeometry(QtCore.QRect(20, 310, 351, 34))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.layoutWidget_2)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_points_4 = QtWidgets.QLabel(self.layoutWidget_2)
        self.label_points_4.setObjectName("label_points_4")
        self.horizontalLayout_3.addWidget(self.label_points_4)
        self.mQgsFileWidget_2 = gui.QgsFileWidget(self.layoutWidget_2)
        self.mQgsFileWidget_2.setFilter("")
        self.mQgsFileWidget_2.setStorageMode(gui.QgsFileWidget.SaveFile)
        self.mQgsFileWidget_2.setObjectName("mQgsFileWidget_2")
        self.horizontalLayout_3.addWidget(self.mQgsFileWidget_2)
        self.formLayoutWidget_2 = QtWidgets.QWidget(VESPERDialogBase)
        self.formLayoutWidget_2.setGeometry(QtCore.QRect(20, 10, 351, 111))
        self.formLayoutWidget_2.setObjectName("formLayoutWidget_2")
        self.formLayout_3 = QtWidgets.QFormLayout(self.formLayoutWidget_2)
        self.formLayout_3.setContentsMargins(0, 0, 0, 0)
        self.formLayout_3.setObjectName("formLayout_3")
        self.label_5 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_5.setObjectName("label_5")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.label_6 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_6.setObjectName("label_6")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.Z = gui.QgsFieldComboBox(self.formLayoutWidget_2)
        self.Z.setObjectName("Z")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.Z)
        self.points = gui.QgsMapLayerComboBox(self.formLayoutWidget_2)
        self.points.setObjectName("points")
        self.formLayout_3.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.points)

        self.retranslateUi(VESPERDialogBase)
        self.button_box.accepted.connect(VESPERDialogBase.accept)
        self.button_box.rejected.connect(VESPERDialogBase.reject)
        QtCore.QMetaObject.connectSlotsByName(VESPERDialogBase)

    def retranslateUi(self, VESPERDialogBase):
        _translate = QtCore.QCoreApplication.translate
        VESPERDialogBase.setWindowTitle(_translate("VESPERDialogBase", "VESPER"))
        self.label.setText(_translate("VESPERDialogBase", "Number of neighbours"))
        self.label_2.setText(_translate("VESPERDialogBase", "Variogram model"))
        self.variogram_model.setItemText(0, _translate("VESPERDialogBase", "Exponential"))
        self.variogram_model.setItemText(1, _translate("VESPERDialogBase", "Spherical"))
        self.label_points_2.setText(_translate("VESPERDialogBase", "Parameters:"))
        self.label_points_4.setText(_translate("VESPERDialogBase", "Output"))
        self.label_5.setText(_translate("VESPERDialogBase", "Select a layer"))
        self.label_6.setText(_translate("VESPERDialogBase", "Z"))
from qgis import gui
