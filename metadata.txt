# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=VESPER
qgisMinimumVersion=3.0
description=Variogram Estimation and Spatial Prediction with ERror
version=0.1.0
author=José Padarian
email=jose.padarian@sydney.edu.au

about=Variogram Estimation and Spatial Prediction with ERror

tracker=https://gitlab.com/spadarian/vesper_qgis/issues
repository=https://gitlab.com/spadarian/vesper_qgis
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=python, interpolation

homepage=https://gitlab.com/spadarian/vesper_qgis
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

Category of the plugin: Raster, Vector, Database or Web
# category=

# If the plugin can run on QGIS Server.
server=False

