# -*- coding: utf-8 -*-
"""
/***************************************************************************
 VESPER
                                 A QGIS plugin
 Variogram Estimation and Spatial Prediction with ERror
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2021-04-07
        copyright            : (C) 2021 by José Padarian
        email                : jose.padarian@sydney.edu.au
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load VESPER class from file VESPER.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .vesper_qgis import VESPER
    return VESPER(iface)
